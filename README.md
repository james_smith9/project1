# PROJECT NAME

## Project Description

An HTML Website that allows employees to login and make reimbursement requests

## Technologies Used

* HTML
* JDBC
* MAVEN
* SQL
* AWS

## Features

* Employees can login
* Employees can submit reimbursement requests

To-do list:
* Allow managers to reject or apporve reimburesement requests
